FROM nginx:1.15.4
RUN apt-get -y update && apt-get -y full-upgrade && apt -y install wget
RUN wget https://github.com/gohugoio/hugo/releases/download/v0.49/hugo_0.49_Linux-64bit.deb
RUN dpkg --install hugo_0.49_Linux-64bit.deb
RUN mkdir hugo
COPY ./ hugo/
RUN cd hugo && hugo -v && cp -r public/* /usr/share/nginx/html
RUN sed -i'' -e s/localhost/\_/g /etc/nginx/conf.d/default.conf
# RUN sed -i'' -e 's/listen.*/listen 8001;/g' /etc/nginx/conf.d/default.conf
RUN rm -rf /hugo



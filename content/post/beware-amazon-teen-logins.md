+++
title = "Beware Amazon Teen Logins"
author = ["C. R. Oldham"]
date = 2018-08-11
lastmod = 2018-10-01T11:26:55-06:00
tags = ["general", "amazon"]
categories = ["general"]
draft = false
weight = 2002
+++

{{< figure src="/ox-hugo/teen-login-just-say-no.png" >}}

If you pay for the privilege of Amazon Prime, Amazon has a new feature they have quietly rolled out that changes the way your family members interact with your Prime Membership.

The tl;dr is that if you are grandfathered into the older method of family management that enabled you to share your Prime benefits with three adults, do not touch your Prime settings. If you change anything you will be migrated to the new Households features and you will not be able to go back.

I’m not sure who at Amazon designed the new Teen Logins feature, but it is a wreck. If you read the basic information on the Teen Logins Parent’s Page it sounds like a great deal, seeming similar to the way Family Sharing works with Apple’s iOS ecosystem. Teens can pay with a parent’s card, and the parent gets a notification to approve or deny the request. They get Prime shipping benefits, and parents can restrict where a teen can ship. Teens can “Shop, stream and explore Amazon from your own login.” But here’s what the page does NOT say:

-   Teens can only access their Teen Login account from the Amazon App on a smartphone.
-   They cannot stream digital video to anything other than a smartphone
-   If a teen already has an Amazon login, to link their existing account to a new Teen Login account, the existing login cannot have any payment methods, nor any digital content.
-   Teen Logins cannot access shared family content.

So for those of us that already had accounts for our kids and had other safeguards in place to protect them, we basically cannot move them to the new Teen Logins feature and at least one member of our family loses access to Prime Shipping.

I was on Chat with Amazon representatives for 45+ minutes this morning. I only found out about these limitations from them, I could not find anything on the Amazon site about it. And the 5 representatives I talked to were either unable or unwilling to switch my account back to the way it was before. They were, however, perfectly willing to suggest that I spend an additional $59.50 per year for the “Prime Student” membership for one of my kids.

I know that people abuse the Prime Membership features, but ironically I was trying to do the right thing when I started this process — I was in the process of removing my oldest from our Amazon account because he was moving out.

My recommendation to Amazon would be to scrap the Teen Login program altogether and switch to simply allowing up to 5 or 6 household members to share a Prime subscription. Maybe only allow the first two members to ship to any address, and the remaining ones to only ship to the primary address. Or, at the very least, remove the draconian restrictions on the Teen program (can’t use a web browser? WHA?!?).

Amazon Teen Logins. Just say “no”.

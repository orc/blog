+++
title = "Removing WireLurker with Salt"
author = ["C. R. Oldham"]
date = 2015-01-24
lastmod = 2018-10-01T11:26:55-06:00
tags = ["macos", "salt", "saltstack", "cve"]
categories = ["tech"]
draft = false
weight = 2003
+++

Claud Xiao from Palo Alto Networks has been in touch with me and I updated this script with his recommendations.

Please note I don’t plan to add Windows support, the anti-malware vendors do a great job maintaining signatures and removing stuff like this.

The news hit the fan early yesterday morning—lots of Apple haters were giddy with excitement at the revelation of the WireLurker trojan that infects iOS devices via their host Macintosh when the devices are plugged in via USB.

Publicized by Palo Alto Networks, details on WireLurker can be found at their website. Helpfully, Palo Alto also published a Python script that can detect the infection. Removing the infection from an iOS device is a matter of backing up the device, erasing it completely by restoring it to factory defaults, and then restoring the backup. Props to Topher Kessler of MacIssues for [documenting this process](http://www.macissues.com/2014/11/06/faq-on-how-to-detect-and-remove-wirelurker-from-os-x-and-ios/).

I took Palo Alto’s script and modified it so it can either be run from the command line or as a Salt execution module. From the command line:

`python wireunlurk.py`

will scan your Mac for signs of WireLurker. `-h` for help (not much there) or `-c` for “clean”.

wireunlurk.py will move any infected files to a dynamically-created directory in `/tmp` that starts with `wireunlurk_bk`.

If you want to run this in your Salt infrastructure, put `wireunlurk.py` in `/srv/salt/_modules` (or equivalent directory if you have customized it) and run the following on your Salt master:

`salt -G 'os:MacOS' saltutil.sync_modules salt -G'os:MacOS' wireunlurk.scan`

Add `clean=True` if you want to clean up the infection as well.

This saved me a significant amount of time scanning my Macs just at home—we have 7 Macs on my home network and rather than ssh’ing to each one, or using a tool like [[<https://github.com/brockgr/csshx>][csshX], as soon as I got the script running and ‘saltified’ I executed the above command and could sleep with peace of mind knowing none of our devices were infected.

You can find my modified script here: <https://github.com/saltstack/salt-contrib/tree/master/modules/wireunlurk>

+++
title = "Installing macOS X 10.9.2 with Salt"
author = ["C. R. Oldham"]
date = 2015-01-24
lastmod = 2018-10-01T11:26:55-06:00
tags = ["macos", "salt", "saltstack"]
categories = ["tech"]
draft = false
weight = 2004
+++

Several weeks ago I installed [Salt](https://github.com/saltstack/salt) on all my Macs. I have 7 currently, two of which cannot run Mavericks and are stuck at Lion (10.7). I know you can configure them to install updates automatically, but a couple of these are development machines and one is a server, and I just don’t like the idea of having them install updates and reboot whenever they feel like it.

Furthermore, the 10.9.2 release contains an important fix—the so called ‘gotofail’ security vulnerablity, fully documented here: <https://www.imperialviolet.org/2014/02/22/applebug.html>. You can check to see if you are vulnerable with <http://gotofail.com>.

I was dreading manually going to each of these machines and running Software Update, waiting for it to figure out if there were really packages to install (why does that take so long, anyway?), and doing the click dance to get it installed.

Enter Salt.

(full disclaimer—I do work for [SaltStack](https://saltstack.com), the company behind open source Salt)

Using Salt turned probably an hour of updating into 3 commands executed at my leisure. Note, I run my salt-master on Ubuntu in a Fusion VM on my Mac Mini server. After downloading the combo updater from Apple’s support site, I mounted it and extracted the .pkg file from it, then copied that file to my Salt master’s `/srv` directory (`/srv/salt/OSXUpd10.9.2.pkg`).

Then:

```text
salt-master# salt -C 'G@os:MacOS and G@osrelease:10.9.1' cp.get_file \ OSXUpd10.9.2.pkg /tmp/OSXUpd10.9.2.pkg salt-master# salt -C 'G@os:MacOS and G@osrelease:10.9.1' cmd.run \ 'installer -pkg /tmp/OSXUpd10.9.2.pkg -target /' salt-master# salt -C 'G@os:MacOS and G@osrelease:10.9.1' cmd.run \ 'shutdown -r now'
```

So what the above says is

1.  For all MacOS machines that are on 10.9.1, copy the package file to the `/tmp` directory on the machine (thus avoiding my Lion machines). The `-C` says this is a [compound target](https://docs.saltstack.com/en/latest/topics/targeting/compound.html), and the command will match against both the os grain (to be “MacOS”) and the osrelease grain (to be “10.9.1").
2.  For those same machines, run Apple’s package utility in unattended mode on the package file, and install that to the boot volume.
3.  Finally, reboot the machine.

The response I got back was identical for each machine, and looks like

```yaml
mini-server:
   installer: Package name is OS X Update
   installer: Installing at base path /
   installer: The install was successful.
   installer: The install requires restarting now.
```

So, did it work? After waiting for the machines to come back up (use salt-run manage.status on the Salt master to see when they are all online again), the following will show the OS release number for all my Macs.

```shell
salt-master# salt -C 'G@os:MacOS' grains.item osrelease

mini-server:
   osrelease:
     10.9.2
imac-01:
  osrelease:
     10.9.2
air-01:
  osrelease:
     10.9.2
mini-01:
  osrelease:
     10.7.5
macbookpro-01:
  osrelease:
     10.9.2
macbookpro-02:
  osrelease:
     10.9.2
white-macbook:
  osrelease:
     10.7.5
```

(Just to be clear, names sanitized)

Voila!

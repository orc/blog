+++
title = "Was this information helpful?"
author = ["C. R. Oldham"]
date = 2016-03-03
lastmod = 2018-10-01T11:26:55-06:00
tags = ["general"]
categories = ["general"]
draft = false
weight = 2001
+++

After helping a friend troubleshoot issues stemming from the difference between Microsoft’s MSI and Click-to-Run installers, I am now convinced that all knowledge base articles that ask for feedback need an additional checkbox.

{{< figure src="/ox-hugo/info-helpful.png" >}}

(background, friend had legal Office 2013 license installed via MSI. Bought Project as a downloadable online. Project uses Click-to-Run installer. This article says that MSI and CTR versions cannot co-exist, and to fix the problem he needs to uninstall Office.)

No wonder Google Docs is taking over the world.
